.. _ref_tutorial:

=========
Tutorials
=========

This section contains self-contained examples of using MAICoS for various tasks. 
The examples are structured in the form of Jupyter notebooks, rendered for 
viewing here and available for interactive execution in the top-level 
``/docs/source/tutorials`` directory of the `repository`_.

.. _repository: https://gitlab.com/maicos-devel/maicos/-/tree/master/docs/source/tutorials


.. toctree::
   :titlesonly:
   :maxdepth: 4
   :caption: Density modules
   
   ../tutorials/density/density_planar
   ../tutorials/density/density_cylinder
   
.. toctree::
   :titlesonly:
   :maxdepth: 4
   :caption: Structure modules
   
   ../tutorials/structure/diporder
   
.. toctree::
   :titlesonly:
   :maxdepth: 4
   :caption: Timeseries modules
   
   ../tutorials/timeseries/dipole_angle
   ../tutorials/timeseries/kinetic_energy
