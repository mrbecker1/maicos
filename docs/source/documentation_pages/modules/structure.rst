=================
Structure modules
=================

.. automodule:: maicos.modules.structure

.. toctree::
   :titlesonly:
   :maxdepth: 4
   :caption: Structure modules
   :hidden:
   
   saxs
   diporder
   debyer

