======
Debyer
======

.. automodule:: maicos.modules.structure.Debye
    :members:
    :undoc-members:
    :show-inheritance:
